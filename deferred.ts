export type Deferred<T> = ReturnType<typeof defer<T>>;

function assertNotUndefined<T>(value: T | undefined): asserts value is T {
  if (value === undefined) {
    throw new Error("value is undefined");
  }
}

export function defer<T>() {
  type PromiseExecutor = ConstructorParameters<typeof Promise<T>>[0];
  let resolve: Parameters<PromiseExecutor>[0] | undefined;
  let reject: Parameters<PromiseExecutor>[1] | undefined;

  const promise = new Promise<T>((yes, no) => {
    resolve = yes;
    reject = no;
  });

  assertNotUndefined(resolve);
  assertNotUndefined(reject);
  return Object.assign(promise, { resolve, reject });
}

export default defer;
