import { describe, expect, it } from "bun:test";
import defer from "./deferred";

describe("deferred", () => {
  it("should resolve", async () => {
    const deferred = defer();
    deferred.resolve("foo");
    const result = await deferred;
    expect(result).toEqual("foo");
  });

  it("should reject", async () => {
    const deferred = defer();
    deferred.reject("foo");
    try {
      await deferred;
    } catch (err) {
      expect(err).toEqual("foo");
    }
  });
});
